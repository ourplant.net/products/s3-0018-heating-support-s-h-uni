Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0018-heating-support-s-h-uni).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         |   |
| assembly drawing         |[de](https://gitlab.com/ourplant.net/products/s3-0018-heating-support-s-h-uni/-/raw/main/02_assembly_drawing/s3-0018-a_heating_support.PDF)                               |
| circuit diagram          |[de](https://gitlab.com/ourplant.net/products/s3-0018-heating-support-s-h-uni/-/raw/main/03_circuit_diagram/S3-0018-EPLAN-B.pdf)                |
| maintenance instructions |                  |
| spare parts              |[de](https://gitlab.com/ourplant.net/products/s3-0018-heating-support-s-h-uni/-/raw/main/05_spare_parts/S3-0018-EVL-A.pdf)             |

